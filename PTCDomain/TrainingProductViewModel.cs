﻿

namespace PTCDomain
{
    using AutoMapper;
    using PTCData;
    using PTCCommon;
    using System;
    using System.Collections.Generic;
    public class TrainingProductViewModel : ViewModelBase
    {

        public TrainingProductViewModel() : base()
        {
            
        }
        public TrainingProduct Entity { get; set; }
        public List<TrainingProduct> Products { get; set; }
        public TrainingProduct SearchEntity { get; set; }

        protected override void Init()
        {
            Products = new List<TrainingProduct>();
            SearchEntity = new TrainingProduct();
            Entity = new TrainingProduct();

            base.Init();
        }

        public override void HandleRequest()
        {

            base.HandleRequest();
        }

        protected override void ResetSearch()
        {
            SearchEntity = new TrainingProduct();
            base.ResetSearch();
        }
        protected override void Get()
        {
            TrainingProductManager mgr = new TrainingProductManager();
            Mapper.CreateMap<TrainingProduct, TrainingProductEntity>();
            Mapper.CreateMap<TrainingProductEntity, TrainingProduct>();
            var products = mgr.Get(Mapper.Map<TrainingProductEntity>(SearchEntity));
            Products = Mapper.Map<List<TrainingProduct>>(products);

            base.Get();
        }

        protected override void Save()
        {
            Mapper.CreateMap<TrainingProduct, TrainingProductEntity>();
            TrainingProductManager mgr = new TrainingProductManager();
            if (Mode == "Add")
            {
                //Add data
                mgr.Insert(Mapper.Map<TrainingProductEntity>(Entity));
            }
            else
            {
                mgr.Update(Mapper.Map<TrainingProductEntity>(Entity));
            }

            ValidationErrors = mgr.ValidationErrors;
            base.Save();
        }
        protected override void Add()
        {
            IsValid = true;
            Entity = new TrainingProduct {
                IntroductionDate = DateTime.Now,
                Price = 0,
                Url = "test"
            };
            base.Add();
        }
        protected override void Edit()
        {
            Mapper.CreateMap<TrainingProduct, TrainingProductEntity>();
            TrainingProductManager mgr = new TrainingProductManager();
            Entity = Mapper.Map<TrainingProduct>(mgr.Get(Convert.ToInt32(EventArgument)));
            base.Edit();
        }
        protected override void Delete()
        {
            Mapper.CreateMap<TrainingProduct, TrainingProductEntity>();
            TrainingProductManager mgr = new TrainingProductManager();
            Entity = new TrainingProduct();
            Entity.ProductId = Convert.ToInt32(EventArgument);

            mgr.Delete(Mapper.Map<TrainingProductEntity>(Entity));

            Get();

            base.Delete();
        }
    }
}
