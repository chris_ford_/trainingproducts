﻿

namespace PTCDomain
{
    using System.ComponentModel.DataAnnotations;
    using System;
    public class TrainingProduct
    {
        public int ProductId { get; set; }

        [Required(ErrorMessage ="Product Name must be filled in.")]
        [Display(Description ="Product Name")]
        [StringLength(150, MinimumLength =4, ErrorMessage ="Product Name must be greater than {2} characters and less than {1} characters.")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Introduction Date must be filled in.")]
        [Display(Description = "Introduction Date")]
        [Range(typeof(DateTime), "1/1/2000", "12/31/2020", ErrorMessage = "Introduction Date must be greater than {1} and less than {2}")]
        public DateTime IntroductionDate { get; set; }

        [Required(ErrorMessage = "URL must be filled in.")]
        [Display(Description = "URL")]
        
        public string Url { get; set; }

        [Range(1,9999, ErrorMessage ="Must be between {1}, and {2}")]
        [Display(Description ="Price")]
        public decimal Price { get; set; }
    }
}
