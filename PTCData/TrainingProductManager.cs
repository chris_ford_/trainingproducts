﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PTCData
{
    public class TrainingProductManager
    {
        private PTCContext db = new PTCContext();
        public TrainingProductManager()
        {
            ValidationErrors = new List<KeyValuePair<string, string>>();
        }
        public List<KeyValuePair<string, string>> ValidationErrors { get; set; }
        public List<TrainingProductEntity> Get(TrainingProductEntity entity)
        {
            List<TrainingProductEntity> ret = db.TrainingProducts.ToList();

            if(!string.IsNullOrEmpty(entity.ProductName))
            {
                ret = ret.FindAll(p => p.ProductName.ToLower().StartsWith(entity.ProductName.ToLower()));
            }
            return ret;

        }

        public void Delete(TrainingProductEntity entity)
        {
            //Delete Code
            db.TrainingProducts.Remove(entity);
        }

        public TrainingProductEntity Get(int productId)
        {
            var ret = new TrainingProductEntity();

            //Call data access
            ret = db.TrainingProducts.Find(productId);

            return ret;
        }

        public void Update(TrainingProductEntity entity)
        {
            bool ret = false;
            ret = Validate(entity);
            if(ret)
            {
                //Update code
                db.Entry(entity).State = EntityState.Modified;
                db.SaveChangesAsync();
            }
        }
        public bool Validate(TrainingProductEntity entity)
        {
            ValidationErrors.Clear();
            if(!string.IsNullOrEmpty(entity.ProductName))
            {
                if(entity.ProductName.ToLower() == entity.ProductName)
                {
                    ValidationErrors.Add(new KeyValuePair<string, string>("ProductName", "Product Name must not be all lower case"));
                }
            }
            return (ValidationErrors.Count == 0);
        }
        public bool Insert(TrainingProductEntity entity)
        {
            bool ret = false;
            ret = Validate(entity);
            if(ret)
            {
                db.TrainingProducts.Add(entity);
                db.SaveChangesAsync();
            }

            return ret;
        }
    }
}
