﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PTCData
{
    public class TrainingProductEntity
    {
        [Key]
        public int ProductId { get; set; }
        public string ProductName { get; set; }

        public DateTime IntroductionDate { get; set; }
        
        public string Url { get; set; }

        public decimal Price { get; set; }
    }
}
