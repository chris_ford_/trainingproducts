﻿namespace PTCData
{
    using System.Data.Entity;
    using System.Data.Entity.ModelConfiguration.Conventions;

    public class PTCContext : DbContext
    {
        public PTCContext() : base("PTCContext")
        {

        }

        public DbSet<TrainingProductEntity> TrainingProducts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
