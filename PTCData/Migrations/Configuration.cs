namespace PTCData.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PTCData.PTCContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            ContextKey = "PTCData.PTCContext";
        }

        protected override void Seed(PTCData.PTCContext context)
        {
            var products = new List<TrainingProductEntity>
            {
                new TrainingProductEntity{ProductName = "Extending Bootstrap with CSS, Javascript and jQuery",
                                    IntroductionDate = Convert.ToDateTime("6/11/2015"),
                                    Url="http://bit.ly/1SNzc0i",
                                    Price=Convert.ToDecimal(29.00)},
                new TrainingProductEntity{ProductName = "Fundumentals of MVC 5",
                                    IntroductionDate = Convert.ToDateTime("6/12/2015"),
                                    Url = "http://bit.ly/1SNzc0i",
                                    Price = Convert.ToDecimal(35.00)},
                new TrainingProductEntity{ProductName = "Introduction to ASP.Net",
                                    IntroductionDate = Convert.ToDateTime("6/11/2012"),
                                    Url = "http://bit.ly/1SNzc0i",
                                    Price = Convert.ToDecimal(29.00)},
                new TrainingProductEntity {ProductName = "TDD",
                                    IntroductionDate = Convert.ToDateTime("6/11/2015"),
                                    Url = "http://bit.ly/1SNzc0i",
                                    Price = Convert.ToDecimal(29.00)}
            };
            products.ForEach(p => context.TrainingProducts.Add(p));
            context.SaveChanges();
        }
    }
}
