﻿
namespace PTC.Controllers
{
    using PTCDomain;
    using System.Collections.Generic;
    using System.Web.Mvc;
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            TrainingProductViewModel viewModel = new TrainingProductViewModel();
            viewModel.HandleRequest();

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Index(TrainingProductViewModel vm)
        {
            vm.IsValid = ModelState.IsValid;
            vm.HandleRequest();

            if(vm.IsValid)
            {
                ModelState.Clear();
            }
            else
            {
                foreach(KeyValuePair<string, string> item in vm.ValidationErrors)
                {
                    ModelState.AddModelError(item.Key, item.Value);
                }
            }
            return View(vm);
        }
    }
}